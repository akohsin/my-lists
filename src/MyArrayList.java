public class MyArrayList implements IMylist<Integer> {

    private Integer[] tablica;

    public void addSorted(Integer data) {


        boolean isAdded = false;
        if (tablica == null) {
            Integer[] bufor = new Integer[1];
            bufor[0] = data;
            tablica = bufor;
        } else {
            Integer[] bufor = new Integer[tablica.length + 1];
            for (int i = 0; i < tablica.length; i++) {
                if (tablica[i] < data) {
                    bufor[i] = data;
                    isAdded = true;
                    for (; i < tablica.length; i++) {
                        bufor[i + 1] = tablica[i];
                    }

                    break;

                } else {
                    bufor[i] = tablica[i];
                }
                if (i == tablica.length - 1) {
                    bufor[tablica.length] = data;
                }
            }
            tablica = bufor;

        }


    }

    public void print() {
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);

        }
    }


    @Override
    public void add(int index, Integer data) {
        if (index > tablica.length) {
            Integer[] bufor = new Integer[index + 1];

            for (int i = 0; i < tablica.length; i++) {
                bufor[i] = tablica[i];
            }
            bufor[index] = data;
            tablica = bufor;
        } else if (index == tablica.length) {
            Integer[] bufor = new Integer[tablica.length + 1];
            for (int i = 0; i < tablica.length; i++) {
                bufor[i] = tablica[i];
            }
            bufor[tablica.length] = data;
            tablica = bufor;
        } else {
            Integer[] bufor = new Integer[tablica.length + 1];
            for (int i = 0; i < index; i++) {
                bufor[i] = tablica[i];
            }
            bufor[index] = data;
            for (int i = index + 1; i < tablica.length + 1; i++) {
                bufor[i] = tablica[i];
            }
            tablica = bufor;

        }


    }

    @Override
    public void add(Integer data) {
        add(tablica.length, data);
    }

    //utworzenie tablicy wiekszej o maks 1.
    public void add2(int index, Integer data) {
        if (index < tablica.length) add(index, data);
        else add(tablica.length, data);
    }

    @Override
    public Node get(int index) {
        return null;
    }
}

