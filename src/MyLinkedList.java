

public class MyLinkedList<T> implements IMylist {
    Node head = new Node();
    private int size = 0;

    @Override
    public void add(Object data) {
        add(size, data);
    }

    @Override
    public void add(int index, Object data) {
        Node n = getNode(index);
        Node n2 = n.next;
        Node n3 = new Node();
        n.next =n3;
        n3.next=n2;
        n3.wartosc=n.wartosc;
        n.wartosc=data;
        size++;
    }

    public void add(int e) {
        add(size, e);
    }

    @Override
    public Object get(int index) {
        return getNode(index).wartosc;
    }


    private Node getNode(int index) {
        Node n = head;
        int i = 0;
        while (true) {
            if (n.next == null) {
                return n;
            }

            if (i++ >= index) {
                return n;
            }

            n = n.next;
        }
    }
}