public interface IMylist<T> {

    void add(T data);

    void add(int index, T data);
    Object get(int index);
    String toString();

}
